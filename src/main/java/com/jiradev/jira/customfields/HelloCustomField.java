package com.jiradev.jira.customfields;

import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;

public class HelloCustomField extends CalculatedCFType {
    private static final Logger log = LoggerFactory.getLogger(HelloCustomField.class);

    public HelloCustomField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
    super();
}

    public Object getValueFromIssue(CustomField field, Issue issue) {
        return "Hello";
    }

    /** not sure what this does...doesn't ever seem to get called...but must exist */
    public String getStringFromSingularObject(Object customFieldObject) {
        return customFieldObject.toString();
    }

    /** not sure what this does...doesn't ever seem to get called...but must exist */
    public Object getSingularObjectFromString(String customFieldObject) {
        return customFieldObject;
    }
}