package ut.com.jiradev.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.jiradev.jira.customfields.HelloCustomField;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class HelloCustomFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //HelloCustomField testClass = new HelloCustomField();

        throw new Exception("HelloCustomField has no tests!");

    }

}
